import 'dart:convert';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:semana3/models/user_model.dart';

class UserData {
  final _database = FirebaseDatabase.instance;

  addUser(Map<String, dynamic> userData) async {
    var ref = _database.ref('users/${userData['id']}');
    await ref.set(userData);
  }

  Future<UserModel> getUser(String userID) async {
    var ref = _database.ref('users/$userID');
    DatabaseEvent event = await ref.once();
    debugPrint(event.snapshot.value.toString());
    return UserModel.fromJSon(jsonDecode(jsonEncode(event.snapshot.value)));
  }
}
