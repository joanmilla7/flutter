import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:semana3/models/country_model.dart';
import 'package:http/http.dart' as http;

class CountryService extends ChangeNotifier {
  final String _baseUrl = 'https://flutter-86343-default-rtdb.firebaseio.com';
  final List<CountryModel> countries = [];
  late CountryModel selectedCountry;
  bool isLoading = true;
  bool isSaving = false;

  CountryService() {
    loadCountries();
  }

  Future<bool> loadCountries() async {
    isLoading = true;

    var client = http.Client();
    final resp = await client.get(Uri.parse(_baseUrl + '/countries.json'));
    final Map<String, dynamic> countriesMap = json.decode(resp.body);
    countries.clear();
    countriesMap.forEach((key, value) {
      final tempCountry = CountryModel.fromMap(value);
      tempCountry.id = key;
      countries.add(tempCountry);
    });
    return true;
  }

  Future<String> createCountry(CountryModel country) async {
    var client = http.Client();
    final resp = await client.post(Uri.parse(_baseUrl + '/countries.json'),
        body: country.toJson());
    final decodedData = json.decode(resp.body);
    country.id = decodedData['name'];

    countries.add(country);

    return country.id!;
  }

  Future saveOrCreateProduct(CountryModel country) async {
    isSaving = true;
    notifyListeners();
    // Es necesario crear
    await this.createCountry(country);
    isSaving = false;
    notifyListeners();
  }
}
