import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:semana3/data/user_data.dart';
import 'package:semana3/models/user_model.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);
  @override
  _RegisterScreen createState() => _RegisterScreen();
}

class _RegisterScreen extends State<RegisterScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String _firstName = '';
  String _lastName = '';
  String _email = '';
  String _password = '';
  String _birthDate = '';
  String _country = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('REGISTRO'),
      ),
      body: Container(
        padding: const EdgeInsets.all(16),
        alignment: Alignment.center,
        child: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                TextFormField(
                  decoration: const InputDecoration(
                    hintText: 'Nombre',
                    label: Text('Nombre'),
                  ),
                  onChanged: (value) {
                    setState(() {
                      _firstName = value;
                    });
                  },
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Este campo no puede quedar vacío';
                    } else {
                      return null;
                    }
                  },
                  keyboardType: TextInputType.name,
                ),
                TextFormField(
                  decoration: const InputDecoration(
                    hintText: 'Apellido',
                    label: Text('Apellido'),
                  ),
                  onChanged: (value) {
                    setState(() {
                      _lastName = value;
                    });
                  },
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Este campo no puede quedar vacío';
                    } else {
                      return null;
                    }
                  },
                  keyboardType: TextInputType.name,
                ),
                TextFormField(
                  decoration: const InputDecoration(
                    hintText: 'Email',
                    label: Text('Email'),
                  ),
                  onChanged: (value) {
                    setState(() {
                      _email = value;
                    });
                  },
                  validator: (value) {
                    var pattern =
                        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                    RegExp regex = RegExp(pattern);
                    if (!regex.hasMatch(value ?? '')) {
                      return 'Email inválido';
                    } else {
                      return null;
                    }
                  },
                  keyboardType: TextInputType.emailAddress,
                ),
                TextFormField(
                  decoration: const InputDecoration(
                    hintText: 'Password',
                    label: Text('Password'),
                  ),
                  onChanged: (value) {
                    setState(() {
                      _password = value;
                    });
                  },
                  validator: (value) {
                    if (value != null && value.length >= 8) {
                      return null;
                    } else {
                      return 'La contraseña debe tener al menos 8 caracteres';
                    }
                  },
                  keyboardType: TextInputType.visiblePassword,
                ),
                TextFormField(
                  decoration: const InputDecoration(
                    hintText: 'Fecha de nacimiento',
                    label: Text('Fecha de nacimiento'),
                  ),
                  onChanged: (value) {
                    setState(() {
                      _birthDate = value;
                    });
                  },
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Este campo no puede quedar vacío';
                    } else {
                      return null;
                    }
                  },
                  keyboardType: TextInputType.number,
                ),
                TextFormField(
                  decoration: const InputDecoration(
                    hintText: 'País',
                    label: Text('País'),
                  ),
                  onChanged: (value) {
                    setState(() {
                      _country = value;
                    });
                  },
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Este campo no puede quedar vacío';
                    } else {
                      return null;
                    }
                  },
                  keyboardType: TextInputType.name,
                ),
                const SizedBox(height: 32),
                TextButton(
                    onPressed: () async {
                      if (_formKey.currentState!.validate()) {
                        try {
                          UserCredential userCredential = await FirebaseAuth
                              .instance
                              .createUserWithEmailAndPassword(
                                  email: _email, password: _password);
                          var snackBar =
                              const SnackBar(content: Text('Registro exitoso'));
                          ScaffoldMessenger.of(context).showSnackBar(snackBar);
                          UserData().addUser(UserModel(
                            id: userCredential.user!.uid,
                            firstName: _firstName,
                            lastName: _lastName,
                            birthDate: _birthDate,
                            email: _email,
                            country: _country,
                            password: _password,
                          ).toJson());
                          await FirebaseAuth.instance.signOut();
                          Navigator.pushReplacementNamed(context, 'login');
                        } on FirebaseAuthException catch (e) {
                          if (e.code == 'weak-password') {
                            print('The password provided is too weak.');
                          } else if (e.code == 'email-already-in-use') {
                            print('The account already exists for that email.');
                            var snackBar =
                                const SnackBar(content: Text('Email en uso'));
                            ScaffoldMessenger.of(context)
                                .showSnackBar(snackBar);
                          }
                        } catch (e) {
                          print(e);
                          var snackBar = const SnackBar(
                              content: Text(
                                  'Se ha producido un error creando el registro'));
                          ScaffoldMessenger.of(context).showSnackBar(snackBar);
                        }
                      }
                    },
                    child: const Text('CREAR REGISTRO')),
                TextButton(
                    onPressed: () {
                      Navigator.pushReplacementNamed(context, 'login');
                    },
                    child: const Text('INGRESAR')),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
