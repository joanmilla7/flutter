import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:semana3/data/user_data.dart';
import 'package:semana3/models/user_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);
  @override
  _LoginScreen createState() => _LoginScreen();
}

class _LoginScreen extends State<LoginScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String _email = '';
  String _password = '';

  _initScreen() async {
    final prefs = await SharedPreferences.getInstance();
    var userData = prefs.getString('user_data');
    if (userData != null && userData.isNotEmpty) {
      try {
        var user = UserModel.fromJSon(jsonDecode(userData));
        Navigator.pushReplacementNamed(context, 'home', arguments: user);
      } catch (error) {
        debugPrint(error.toString());
      }
    } else {
      FirebaseAuth.instance.authStateChanges().listen((User? user) {
        if (user == null) {
          print('User is currently signed out!');
        } else {
          print('User is signed in!');
        }
      });
    }
  }

  @override
  void initState() {
    _initScreen();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('INGRESO'),
      ),
      body: Container(
        padding: const EdgeInsets.all(16),
        alignment: Alignment.center,
        child: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                TextFormField(
                  decoration: const InputDecoration(
                    hintText: 'Email',
                    label: Text('Email'),
                  ),
                  onChanged: (value) {
                    setState(() {
                      _email = value;
                    });
                  },
                  validator: (value) {
                    var pattern =
                        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                    RegExp regex = RegExp(pattern);
                    if (!regex.hasMatch(value ?? '')) {
                      return 'Email inválido';
                    } else {
                      return null;
                    }
                  },
                  keyboardType: TextInputType.emailAddress,
                ),
                TextFormField(
                  decoration: const InputDecoration(
                    hintText: 'Password',
                    label: Text('Password'),
                  ),
                  onChanged: (value) {
                    setState(() {
                      _password = value;
                    });
                  },
                  validator: (value) {
                    if (value != null && value.length >= 8) {
                      return null;
                    } else {
                      return 'La contraseña debe tener al menos 8 caracteres';
                    }
                  },
                  keyboardType: TextInputType.visiblePassword,
                ),
                const SizedBox(height: 32),
                TextButton(
                    onPressed: () async {
                      if (_formKey.currentState!.validate()) {
                        try {
                          UserCredential userCredential = await FirebaseAuth
                              .instance
                              .signInWithEmailAndPassword(
                                  email: _email, password: _password);
                          var user = await UserData()
                              .getUser(userCredential.user!.uid);
                          await FirebaseAuth.instance.signOut();
                          final prefs = await SharedPreferences.getInstance();
                          await prefs.setString('user_data', jsonEncode(user));
                          Navigator.pushReplacementNamed(context, 'home',
                              arguments: user);
                        } on FirebaseAuthException catch (e) {
                          if (e.code == 'user-not-found') {
                            print('No user found for that email.');
                            var snackBar = SnackBar(
                                content: Text('Usuario no registrado'));
                            ScaffoldMessenger.of(context)
                                .showSnackBar(snackBar);
                          } else if (e.code == 'wrong-password') {
                            print('Wrong password provided for that user.');
                            var snackBar =
                                SnackBar(content: Text('Password inválido'));
                            ScaffoldMessenger.of(context)
                                .showSnackBar(snackBar);
                          }
                        }
                      }
                    },
                    child: const Text('INGRESAR')),
                TextButton(
                  onPressed: () {
                    Navigator.pushReplacementNamed(context, 'register');
                  },
                  child: const Text(
                    'REGISTRARME',
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
