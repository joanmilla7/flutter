
import 'package:flutter/material.dart';
import 'package:semana3/models/country_model.dart';

class CountriesListScreen extends StatelessWidget {
  final List<CountryModel> countries;
  const CountriesListScreen({Key? key, required this.countries})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Center(child: Text('Lista de Países')),
          backgroundColor: Colors.indigo,
        ),
        body: ListView.separated(
            itemBuilder: (context, index) => ListTile(
                  title: Text(countries[index].name),
                  trailing: const Icon(Icons.arrow_forward_ios),
                  onTap: () {
                    Navigator.pushNamed(context, 'detail',
                        arguments: CountryModel(
                            name: countries[index].name,
                            flag: countries[index].flag,
                            currency: countries[index].currency,
                            lt: countries[index].lt,
                            lg: countries[index].lg));
                  },
                ),
            separatorBuilder: (context, index) => const Divider(),
            itemCount: countries.length));
  }
}