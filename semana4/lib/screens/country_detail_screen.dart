import 'package:flutter/material.dart';
import 'package:semana3/models/country_model.dart';

class CountryDetailScreen extends StatelessWidget {
  const CountryDetailScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final country = ModalRoute.of(context)!.settings.arguments as CountryModel;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Detalle País'),
        backgroundColor: Colors.indigo,
      ),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('Nombre: ${country.name}', style: TextStyle(fontSize: 30)),

            SizedBox(height: 20),

            Image(
              image: NetworkImage(country.flag),
              width: 150,
            ),

            SizedBox(height: 20),

            Text('Moneda: ${country.currency}', style: TextStyle(fontSize: 30)),

            SizedBox(height: 20),

            Text('Latitud: ${country.lt}', style: TextStyle(fontSize: 30)),

            SizedBox(height: 20),
            
            Text('Longitud: ${country.lg}', style: TextStyle(fontSize: 30)),
          ]
        ),
      )
    );
  }
}
