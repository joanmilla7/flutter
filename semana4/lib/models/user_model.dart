class UserModel {
  final String id;
  final String firstName;
  final String lastName;
  final String birthDate;
  final String email;
  final String country;
  final String password;

  UserModel({
    required this.id,
    required this.firstName,
    required this.lastName,
    required this.birthDate,
    required this.email,
    required this.country,
    required this.password,
  });

  factory UserModel.fromJSon(Map<String, dynamic> json) {
    return UserModel(
      id: json['id'],
      firstName: json['firstName'],
      lastName: json['lastName'],
      birthDate: json['birthDate'],
      email: json['email'],
      country: json['country'],
      password: '',
    );
  }

  Map<String, dynamic> toJson() =>{
    'id': id,
    'firstName': firstName,
    'lastName': lastName,
    'birthDate': birthDate,
    'email': email,
    'country': country,
    'password': password,
  };
}
