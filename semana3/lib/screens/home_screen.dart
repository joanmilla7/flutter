import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:semana3/models/country_model.dart';
import 'package:semana3/screens/countries_form_screen.dart';
import 'package:semana3/screens/countries_list_screen.dart';

import '../services/country_service.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int _selectedIndex = 0;
  final List<CountryModel> _countries = [];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
    
  }

  _loadData() {
    final countryService = Provider.of<CountryService>(context, listen: false);
    countryService.loadCountries().then((value) {
      setState(() {
        _countries.clear();
        _countries.addAll(countryService.countries);
      });
    });
  }

  @override
  void initState() {
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      _loadData();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: [
          CountriesListScreen(countries: _countries),
          CountriesFormScreen(),
        ].elementAt(_selectedIndex),
        bottomNavigationBar: BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.business),
              label: 'Listado',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.school),
              label: 'Registrar País',
            )
          ],
          currentIndex: _selectedIndex,
          selectedItemColor: Colors.indigo[800],
          onTap: _onItemTapped,
        ));
  }
}