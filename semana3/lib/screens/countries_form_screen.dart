import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:semana3/models/country_model.dart';
import 'package:semana3/providers/country_form_provider.dart';
import 'package:semana3/services/country_service.dart';

class CountriesFormScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final countryService = Provider.of<CountryService>(context);
    countryService.selectedCountry =
        CountryModel(currency: '', name: '', lg: 0, lt: 0, flag: '');
    return ChangeNotifierProvider(
      create: (_) => CountryFormProvider(countryService.selectedCountry),
      child: _CountryScreenBody(countryService: countryService),
    );
  }
}

class _CountryScreenBody extends StatelessWidget {
  _CountryScreenBody({
    Key? key,
    required this.countryService,
  }) : super(key: key);

  CountryService countryService;

  @override
  Widget build(BuildContext context) {
    final countryForm = Provider.of<CountryFormProvider>(context);
    final country = countryForm.country;
    return Scaffold(
      appBar: AppBar(
        title: const Center(child: Text('Registrar País')),
        backgroundColor: Colors.indigo,
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 20),
        child: Form(
          autovalidateMode: AutovalidateMode.onUserInteraction,
          key: countryForm.formKey,
          child: Column(
            children: [
              TextFormField(
                autocorrect: false,
                decoration: const InputDecoration(
                    labelText: "Nombre: ", hintText: "Venezuela"),
                initialValue: country.name,
                onChanged: (value) => country.name = value,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'El campo es requerido';
                  }
                  return null;
                },
              ),
              const SizedBox(height: 15),
              TextFormField(
                decoration: const InputDecoration(labelText: "Bandera: "),
                initialValue: country.flag,
                onChanged: (value) => country.flag = value,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'El campo es requerido';
                  }

                  String pattern =
                      r'(http|https)://[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:/+#-]*[\w@?^=%&amp;/+#-])?';
                  RegExp regExp = new RegExp(pattern);
                  if (!regExp.hasMatch(value)) {
                    return 'Ingresa una url valida';
                  }

                  return null;
                },
              ),
              const SizedBox(height: 15),
              TextFormField(
                  decoration: const InputDecoration(labelText: "Moneda: "),
                  initialValue: country.currency,
                  onChanged: (value) => country.currency = value,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'El campo es requerido';
                    }
                    return null;
                  }),
              const SizedBox(height: 15),
              TextFormField(
                  keyboardType: TextInputType.number,
                  decoration: const InputDecoration(
                    labelText: "Latitud: ",
                  ),
                  initialValue: null,
                  onChanged: (value) => country.lt = int.parse(value),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'El campo es requerido';
                    }
                    if (double.tryParse(value) == null) {
                      return 'Ingresa un numero valido';
                    }
                    return null;
                  }),
              const SizedBox(height: 15),
              TextFormField(
                  keyboardType: TextInputType.number,
                  decoration: const InputDecoration(labelText: "Longitud: "),
                  initialValue: null,
                  onChanged: (value) => country.lg = int.parse(value),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'El campo es requerido';
                    }
                    if (double.tryParse(value) == null) {
                      return 'Ingresa un numero valido';
                    }
                    return null;
                  }),
              const SizedBox(height: 50),
              ElevatedButton(
                  child: const Text("Guardar"),
                  onPressed: () async {
                    //loadData();
                    //countryService.saveOrCreateProduct(countryForm.country);
                    if(countryForm.isValidForm()){
                      countryService.saveOrCreateProduct(countryForm.country);
                      showDialog<String>(
                        context: context,
                        builder: (BuildContext context) => AlertDialog(
                              title: const Text('Registrar'),
                              content: const Text(
                                  'El país ha sido registado con exito'),
                              actions: <Widget>[
                                TextButton(
                                  onPressed: () => Navigator.pop(context, 'OK'),
                                  child: const Text('OK'),
                                ),
                              ],
                        ));
                    }
                    
                  })
            ],
          ),
        ),
      ),
    );
  }
}
