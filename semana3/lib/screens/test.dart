import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:provider/provider.dart';
import 'package:semana3/providers/country_form_provider.dart';
import 'package:semana3/services/country_service.dart';




class TestScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    final productService = Provider.of<CountryService>(context);

    return ChangeNotifierProvider(
      create: ( _ ) => CountryFormProvider( productService.selectedCountry ),
      child: _ProductScreenBody(productService: productService),
    );
  }
}

class _ProductScreenBody extends StatelessWidget {
  const _ProductScreenBody({
    Key? key,
    required this.productService,
  }) : super(key: key);

  final CountryService productService;

  @override
  Widget build(BuildContext context) {
    
    final productForm = Provider.of<CountryFormProvider>(context);

    return Scaffold(
      body: SingleChildScrollView(
        // keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
        child: Column(
          children: [

            Stack(
              children: [
                
                Positioned(
                  top: 60,
                  right: 20,
                  child: IconButton(
                    onPressed: (){}, 
                    icon: Icon( Icons.camera_alt_outlined, size: 40, color: Colors.white ),
                  )
                )
              ],
            ),

            _ProductForm(),

            SizedBox( height: 100 ),

          ],
        ),
      ),

      floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
      floatingActionButton: FloatingActionButton(
        child: productService.isSaving 
          ? CircularProgressIndicator( color: Colors.white )
          : Icon( Icons.save_outlined ),
        onPressed: productService.isSaving 
          ? null
          : () async {
          
          await productService.saveOrCreateProduct(productForm.country);

        },
      ),
   );
  }
}

class _ProductForm extends StatelessWidget {


  @override
  Widget build(BuildContext context) {

    final productForm = Provider.of<CountryFormProvider>(context);
    final product = productForm.country;

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 10 ),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 20),
        width: double.infinity,
        decoration: _buildBoxDecoration(),
        child: Form(
          key: productForm.formKey,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          child: Column(
            children: [

              SizedBox( height: 10 ),

              TextFormField(
                initialValue: product.name,
                onChanged: ( value ) => product.name = value,
                validator: ( value ) {
                  if ( value == null || value.length < 1 )
                    return 'El nombre es obligatorio'; 
                },
                
              ),

              TextFormField(
                autocorrect: false,
                decoration: const InputDecoration(
                    labelText: "Nombre: ", hintText: "Venezuela"),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'El campo es requerido';
                  }
                  return null;
                },
              ),
              const SizedBox(height: 15),
              TextFormField(
                decoration: const InputDecoration(labelText: "Bandera: "),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'El campo es requerido';
                  }

                  String pattern =
                      r'(http|https)://[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:/+#-]*[\w@?^=%&amp;/+#-])?';
                  RegExp regExp = new RegExp(pattern);
                  if (!regExp.hasMatch(value)) {
                    return 'Ingresa una url valida';
                  }

                  return null;
                },
              ),
              const SizedBox(height: 15),
              TextFormField(
                decoration: const InputDecoration(labelText: "Moneda: "),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'El campo es requerido';
                  }
                  return null;
                }
              ),
              const SizedBox(height: 15),
              TextFormField(
                keyboardType: TextInputType.number,
                decoration: const InputDecoration(
                  labelText: "Latitud: ",
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'El campo es requerido';
                  }
                  if (double.tryParse(value ) == null) {
                    return 'Ingresa un numero valido';
                  }
                  return null;
                }
              ),
              const SizedBox(height: 15),
              TextFormField(
                keyboardType: TextInputType.number,
                decoration: const InputDecoration(labelText: "Longitud: "),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'El campo es requerido';
                  }
                  if (double.tryParse(value ) == null) {
                    return 'Ingresa un numero valido';
                  }
                  return null;
                }
              ),
              const SizedBox(height: 50),

            ],
          ),
        ),
      ),
    );
  }

  BoxDecoration _buildBoxDecoration() => BoxDecoration(
    color: Colors.white,
    borderRadius: BorderRadius.only( bottomRight: Radius.circular(25), bottomLeft: Radius.circular(25)),
    boxShadow: [
      BoxShadow(
        color: Colors.black.withOpacity(0.05),
        offset: Offset(0,5),
        blurRadius: 5
      )
    ]
  );
}