import 'dart:convert';

class CountryModel {
  CountryModel({
    required this.currency,
    required this.flag,
    required this.lg,
    required this.lt,
    required this.name,
  });

  String currency;
  String flag;
  int lg;
  int lt;
  String name;
  String? id;

  factory CountryModel.fromJson(String str) =>
      CountryModel.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory CountryModel.fromMap(Map<String, dynamic> json) => CountryModel(
        currency: json["currency"],
        flag: json["flag"],
        lg: json["lg"],
        lt: json["lt"],
        name: json["name"],
      );

  Map<String, dynamic> toMap() => {
        "currency": currency,
        "flag": flag,
        "lg": lg,
        "lt": lt,
        "name": name,
      };
}
