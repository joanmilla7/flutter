import 'package:flutter/material.dart';
import 'package:semana3/models/country_model.dart';

class CountryFormProvider extends ChangeNotifier {

  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  CountryModel country;

  CountryFormProvider( this.country );

  updateAvailability( bool value ) {
    notifyListeners();
  }


  bool isValidForm() {

    print( formKey.currentState?.validate() );
    
    
    return formKey.currentState?.validate() ?? false;
  }

}