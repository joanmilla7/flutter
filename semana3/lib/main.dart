import 'package:flutter/material.dart';
import 'package:semana3/screens/country_detail_screen.dart';
import 'package:semana3/screens/home_screen.dart';
import 'package:provider/provider.dart';
import 'package:semana3/services/country_service.dart';

void main() {
  runApp( App());
}

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => CountryService() )
      ],
      child: const MyApp()
    );
  }
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  // This widget is the root of your application.
  int selectedPage = 0;
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Material App',
      initialRoute: 'home',
      routes: {
        'home': (BuildContext context) => const HomeScreen(),
        'detail': (BuildContext context) => const CountryDetailScreen()
      },
    );
  }
}