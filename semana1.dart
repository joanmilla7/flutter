import 'dart:convert';

void main() {
  
  sortCountries();
    
}


class CountryModel{
  
  String name = '';
  
  String capital = '';
  
  dynamic languages;
  
  dynamic currencies;
  
  CountryModel( this.name, this.capital , this.languages, this.currencies );
  
  CountryModel.Json( Map < String, dynamic > data ) {
    
    name = data[ "name" ];
    
    capital = data[ "capital" ];
    
    languages = data[ "languages" ];
    
    currencies = data[ "currencies" ];
  }
  
  toString(){
    return 
      'name: ${this.name}, city: ${this.capital }, currencies: ${this.currencies}, languages: ${this.languages}';
  }
  
}

void sortCountries(){
  
  const String arrayCountries = '[{"name":"Ecuador","capital":"Quito","languages":"Spanish","currencies":"USD"},{"name":"Western Sahara","capital":"El Aaiún","languages":"Berber","currencies":"DZD"},{"name":"Colombia","capital":"Bogotá","languages":"Spanish","currencies":"COP"},{"name":"Nicaragua","capital":"Managua","languages":"Spanish","currencies":"NIO"},{"name":"Mexico","capital":"Mexico City","languages":"Spanish","currencies":"MXN"},{"name":"Panama","capital":"Panama City","languages":"Spanish","currencies":"PAB"},{"name":"Costa Rica","capital":"San José","languages":"Spanish","currencies":"CRC"},{"name":"Chile","capital":"Santiago","languages":"Spanish","currencies":"CLP"},{"name":"Argentina","capital":"Buenos Aires","languages":"Spanish","currencies":"ARS"},{"name":"Belize","capital":"Belmopan","languages":"Belizean Creole","currencies":"BZD"}]';
    
  List< CountryModel > countries = [];
  
  for ( Map< String, dynamic > country in jsonDecode( arrayCountries )){
    countries.add( CountryModel.Json (
      {
        "name":       country[ "name" ], 
        "capital":   country[ "capital" ],
        "languages":  country[ "languages" ],
        "currencies": country[ "currencies" ]
      }
    ));
  }
  
  countries.sort( ( a, b ) => a.name.compareTo( b.name ) );
  
  for( final country in  countries ){
    print( country ); 
  }
  
}
